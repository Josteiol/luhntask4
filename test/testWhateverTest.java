import static org.junit.jupiter.api.Assertions.*;

import Algorithm.Luhn;
import org.junit.jupiter.api.Test;

public class testWhateverTest{
    Luhn luhn = new Luhn();

    // Test complete algorithm
    @Test
    public void luhnAlgorithmShouldReturnTrue(){
        assertTrue(Luhn.luhn("79927398713"));
    }

    @Test
    public void luhnAlgorithmShouldReturnFalse(){
        assertFalse(Luhn.luhn("79927398712"));
    }

    // Testing checksum extraction
    @Test
    public void extractChecksumShouldBeTwo(){
        assertEquals(2, Luhn.extractChecksum(Luhn.makeArray("79927398712")));
    }

    @Test
    public void extractChecksumShouldBeFour(){
        assertEquals(4, Luhn.extractChecksum(Luhn.makeArray("79927398714")));
    }

    // Testing array creation
    @Test
    public void arrayShouldBeSevenTwoTreeFour(){
        int[] expected = {7,2,3,4};
        assertArrayEquals(expected,Luhn.makeArray("7234"));
    }

    @Test
    public void arrayShouldBeOneTwoTreeFourFiveSixSeven(){
        int[] expected = {1,2,3,4,5,6,7};
        assertArrayEquals(expected,Luhn.makeArray("1234567"));
    }


    // Testing doubling every other value
    @Test
    public void arrayShoulBeSevenEightNineFour(){
        int[] input = {7,4,9,2,7};
        int[] expected = {7,8,9,4};
        assertArrayEquals(expected, Luhn.doubleEveryOther(input));
    }
    @Test
    public void arrayShouldBeSevenSeven(){
        int[] input = {7,8,3};
        int[] expected = {7,7};
        assertArrayEquals(expected, Luhn.doubleEveryOther(input));
    }



    // Testing if input is 16 digits
    @Test
    public void returnShouldBeTrue(){
        assertTrue(Luhn.isSixteenDigits("1234567891234567"));

    }
    @Test
    public void returnShouldBeFalse(){
        assertFalse(Luhn.isSixteenDigits("123456"));
    }

}