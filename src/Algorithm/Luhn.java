package Algorithm;

import java.util.Arrays;

public class Luhn {

    // Complete algorithm to check final sum against the provided checksum
    public static boolean luhn(String originalNumber){

        // Splitting the input and placing it in an array
        int[] originalArray = makeArray(originalNumber);

        // Extracting the check digit
        int checkDigit = extractChecksum(originalArray);

        // Processing every other value
        int[] processedArray = doubleEveryOther(originalArray);

        int sum = processArray(processedArray);

        return  sum == checkDigit;
    }

    // Make a string array
    public static int[] makeArray(String originalNumber){
        int[] originalArray = new int[originalNumber.length()];
        for (int i = 0; i <= originalNumber.length()-1; i++)
        {
            originalArray[i] =  originalNumber.charAt(i) -'0';

        }

        return originalArray;
    }

    public static int extractChecksum(int[] inputArray){
        return inputArray[inputArray.length-1];

    }

    // Double every other value starting from the last value
    public static int[] doubleEveryOther(int[] inputArray){

        int[] processedArray = new int[inputArray.length - 1];

        for(int i = inputArray.length - 2; i >= 0; i--){
            if(i%2 != 0){
                if(inputArray[i]*2 > 9){
                    String number = Integer.toString(inputArray[i] * 2);
                    int number1 = number.charAt(0) - '0';
                    int number2 = number.charAt(1) - '0';
                    processedArray[i] = (number1 + number2);
                }
                else{
                    processedArray[i] = inputArray[i]*2;
                }
            }
            else{
                processedArray[i] = inputArray[i];
            }
        }

        return processedArray;
    }

    // Checking if the array has 16 values
    public static boolean isSixteenDigits(String inputArray){
        if(inputArray.length() == 16){
            return true;
        }else {
            return  false;
        }
    }

    // Calculates the sum to be checked
    public static int processArray(int[] inputArray){
        int sum = 0;

        for(int i = 0; i <= inputArray.length - 1; i++){
            sum += inputArray[i];
        }

        sum = sum * 9;
        sum = sum%10;

        return sum;
    }

}
