import Algorithm.Luhn;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    // TestValue 79927398713 74927 783
    public static void main(String[] args){

        // Taking user input
        System.out.println("Please enter sum");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();


        // Print out separated input
        System.out.println("Input: " + Arrays.toString(Luhn.makeArray(input)) + " " + Luhn.extractChecksum(Luhn.makeArray(input)));

        // Provided checksum
        System.out.println("Expected: "+ Luhn.processArray(Luhn.doubleEveryOther(Luhn.makeArray(input))));

        // Expected checksum
        System.out.println("Provided: "+Luhn.extractChecksum(Luhn.makeArray(input)));

        // If checksum is valid
        System.out.println("Checksum is valid: "+Luhn.luhn(input));

        // If provided digit is 16 digits
        System.out.println(input.length()+" digits. Is credit card:  "+Luhn.isSixteenDigits(input));
    }
}
